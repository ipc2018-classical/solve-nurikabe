#ifndef ASP_H
#define ASP_H

#include "api.h"
#include "smodels.h"

#include <functional>
#include <vector>

class Island;
class Map;

using Painting = std::vector<Island>;

class ASP {
    Smodels smodels;
    const Map &map;
    int num_models;
    int num_valid;

    std::vector<Atom *> black_cells;
    std::vector<Atom *> w_connected;
    std::vector<Atom *> white_cells;
    std::vector<Atom *> numbered_cells;
    std::vector<Atom *> sqr_black;
    std::vector<Atom *> at_least_two_black_neighbours;
    std::vector<std::vector<Atom *>> connected_black;
    std::vector<std::vector<Atom *>> connected_white;
    std::vector<Atom *> aux_atoms;

    void setup_atoms(Api &api); // helper function used by create_program
    void create_program(Api &api);

public:
    ASP(const Map &map);

    Painting next_consistent_painting();
};

#endif /* ASP_H */
