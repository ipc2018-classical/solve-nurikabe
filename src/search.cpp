#include "Map.h"
#include "island.h"
#include "search.h"
#include <bitset>
#include <iostream>
#include <fstream>
#include <queue>
#include <sstream>
#include <tuple>
#include <vector>
#include <limits>


using namespace std;

const int MAX_NUM_ISLANDS = 64;

struct State {
    bitset<MAX_NUM_ISLANDS> visited_islands;
    int robot_position;

    bool operator==(const State &other) const {
        return robot_position == other.robot_position && visited_islands == other.visited_islands;
    }
};

struct SearchNode {
    int g;
    int f;
    int parent_state_island;
    State state;
};

class StateHash {
public:
    size_t operator()(const State &s) const {
        hash<bitset<MAX_NUM_ISLANDS>> hash_fn;
        return s.robot_position ^ hash_fn(s.visited_islands);
    }
};

class Greater {
public:
    bool operator()(const SearchNode &n1, const SearchNode &n2) const {
	    return n1.f > n2.f;
    }
};

void PlanManager::save_plan(const std::string &plan, int cost) {
    if (cost < best_plan_cost) {
        best_plan_cost = cost;
        cout << "found goal with lower goal distance (" << cost << ")" << endl << plan << endl;

        ostringstream filename;
        filename << plan_filename << "." << num_plans;
        ++num_plans;
        ofstream outfile(filename.str());
        outfile << plan << endl
                << "; cost = " << cost << endl;
        outfile.close();
    } else {
        cout << "optimal plan for this painting is not better (" << cost
             << ") than previous best plan (" << best_plan_cost << ") " << endl;
    }
}

int h(const std::vector<Island> &islands, const State &state) {
    int heuristic_value = 0;
    for (int i = 0; i < islands.size() - 1; ++i) {
        if (!state.visited_islands[i]) {
            heuristic_value += islands[i].paint_cost() + 1;
        }
    }
    return heuristic_value;
}

int Search::search(const std::vector<Island> &islands, const Map &map, int robot_index, double w) {
    if (islands.size() > MAX_NUM_ISLANDS) {
        cerr << "The search is currently limited to instances with at most " << MAX_NUM_ISLANDS
             << " islands. (Increase MAX_NUM_ISLANDS in the code.)" << endl;
        exit(1);
    }

    vector<int> island_by_index(map.get_size(), -1);
    for (size_t i = 0; i < islands.size(); ++i) {
        const vector<int> &route = islands[i].get_possible_routes()[0];
        for (int index : route) {
            island_by_index[index] = i;
        }
    }

    SearchNode initial_node;
    initial_node.state.visited_islands.reset();
    initial_node.state.robot_position = robot_index;
    initial_node.parent_state_island = -1;
    initial_node.g = 0;
    initial_node.f = initial_node.g + w * h(islands, initial_node.state);

    priority_queue<SearchNode, vector<SearchNode>, Greater> queue;
    unordered_map<State, tuple<int, int>, StateHash> closed;
    queue.push(move(initial_node));

    int best_f = 0;
    cout << "starting search..." << endl;
    while (!queue.empty()) {
        const SearchNode &entry = queue.top();
        int cost = entry.g;
        const State state = move(entry.state);
		int last_island = entry.parent_state_island;
		int current_island = island_by_index[state.robot_position];
		if (!state.visited_islands[current_island]) current_island = -1;
		queue.pop();

        if (entry.f > best_f) {
            cout << "f layer: " << entry.f << endl;
            best_f = entry.f;
        }

        if (closed.find(state) != closed.end()) {
			continue;
		}
        closed[state] = make_pair(cost, last_island);
        if (is_goal(state, islands.size())) {
            stringstream plan;
            extract_plan(map, islands, robot_index, closed, state, current_island, plan);
            plan_manager.save_plan(plan.str(), cost);
            return cost;
        }

        for (size_t i = 0; i < islands.size(); ++i) {
            if (!state.visited_islands[i]) { // island i not yet painted
                // create one successor for each possible exit point of the
                // island. The successor represents moving to the entry point
                // and painting the island.

                int dest = islands[i].entry_point();
                int dist_to = shortest_path_cost(islands, state.robot_position, dest, state, map);
				if (dist_to == -1) continue; // islands[i] unreachable
                const vector<int> &exits = islands[i].possible_exits();
                for (int exit : exits) {
                    SearchNode succ_node;
                    succ_node.state = state;
                    succ_node.state.visited_islands[i] = true;
                    succ_node.state.robot_position = exit;
                    succ_node.parent_state_island = current_island;
                    succ_node.g = cost + dist_to + islands[i].paint_cost();
                    succ_node.f = succ_node.g + w * h(islands, succ_node.state);
                    queue.push(move(succ_node));
//					cout << "pushed with cost " << cost + dist_to + islands[i].paint_cost() << endl;
                }
            }
        }
    }
    cout << "Goal not reachable" << endl;
    return numeric_limits<int>::max();
}

bool Search::is_goal(const State &state, int num_islands) {
    return state.visited_islands.count() == num_islands;
}

void Search::extract_plan(const Map &map, const std::vector<Island> &islands, int start_pos,
	unordered_map<State, tuple<int, int>, StateHash>& closed,
	const State &state, int current_island, stringstream &plan) {

	int cost, last_island;
	tie(cost, last_island) = closed[state];
	const vector<vector<int>> &routes = islands[current_island].get_possible_routes();
	State prev_state(state);
	prev_state.visited_islands[current_island] = false;
	if (last_island == -1) {
		vector<int> path;
		int dist = shortest_path_cost(islands, start_pos, routes[0][0], prev_state, map, &path);
		for (size_t j = 0; j < path.size() - 1; ++j) {
			plan << "(move pos-" << map.x_coord(path[j]) << "-" << map.y_coord(path[j]) << " pos-"
				 << map.x_coord(path[j+1]) << "-" << map.y_coord(path[j+1]) << ")" << endl;
		}
	} else {
		for (auto exit : islands[last_island].possible_exits()) {
			vector<int> path;
			prev_state.robot_position = exit;
			int prev_cost = get<0>(closed[prev_state]);
			int dist = shortest_path_cost(islands, exit, routes[0][0], prev_state, map, &path);
			if (prev_cost + dist + islands[current_island].paint_cost() == cost) {
				extract_plan(map, islands, start_pos, closed, prev_state, last_island, plan);
				for (size_t j = 0; j < path.size() - 1; ++j) {
					plan << "(move pos-" << map.x_coord(path[j]) << "-" << map.y_coord(path[j]) << " pos-"
						 << map.x_coord(path[j+1]) << "-" << map.y_coord(path[j+1]) << ")" << endl;
				}
				break;
			}
		}
	}

    // painting of current island
    const vector<int> *p_route = nullptr;
    for (const vector<int> &r : routes) {
        if (r.back() == state.robot_position) {
            p_route = &r;
            break;
        }
    }
    assert(p_route);
    const vector<int> &route = *p_route;

	int x_from = map.x_coord(route[0]);
	int y_from = map.y_coord(route[0]);
	int x_to, y_to;
	int remaining = route.size();
	plan << "(start-painting pos-" << x_from << "-" << y_from << " g" << current_island << " n" << remaining << " n" << remaining - 1 <<  ")" << endl;
	--remaining;
	for (size_t i = 1; i < route.size(); ++i) {
		x_to = map.x_coord(route[i]);
		y_to = map.y_coord(route[i]);
		plan << "(move-painting pos-" << x_from << "-" << y_from << " pos-" << x_to << "-" << y_to << " g" << current_island << " n" << remaining << " n" << remaining - 1 << ")" << endl;
		--remaining;
		x_from = x_to;
		y_from = y_to;
	}
	plan << "(end-painting g" << current_island <<  ")" << endl;
}


// basically breadth-first search
int Search::shortest_path_cost(const std::vector<Island> &islands,
	int index_from, int index_to, const State &state, const Map &map,
	vector<int> *path) {

    int map_size = map.get_size();
    vector<int> painted(map_size);
    for (size_t i = 0; i < islands.size(); ++i) {
        if (state.visited_islands[i]) {
            const vector<std::vector<int>> &routes = islands[i].get_possible_routes();
            for (int index : routes[0]) {
                painted[index] = 1;
            }
        }
    }
    vector<int> distance(map_size, -1);
    queue<pair<int,int>> queue;
    queue.push(make_pair(0, index_from));

    while (!queue.empty()) {
        pair<int, int> entry = queue.front();
		queue.pop();
        int dist = entry.first;
        int pos = entry.second;
        if (pos == index_to) {
			if (path != NULL) {
				path->resize(dist + 1);
				path->back() = pos;
				int curr_dist = dist;
				int curr_pos = pos;	
				while (curr_dist > 0) {
                    map.neighbours(curr_pos, neighbors);
					for (int n: neighbors) {
						if (distance[n] == curr_dist -1) {
							(*path)[curr_dist - 1] = n;
							--curr_dist;
							curr_pos = n;
							break;
						}
					}
				}
			}
			return dist;
		}
        if (distance[pos] == -1) {  // has not been expanded
            distance[pos] = dist;
            map.neighbours(pos, neighbors);
            for (int n: neighbors) {
                if (!painted[n])
                    queue.push(make_pair(dist + 1, n));
            }
        }
    }
    return -1; // index_to unreachable
}

PlanManager *g_plan_manager = nullptr;
