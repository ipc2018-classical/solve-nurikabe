#ifndef MAP_H
#define MAP_H
#include "island.h"
#include "smodels.h"
#include <tuple>
#include <vector>

enum Entry { BLACK = -1, EMPTY = 0 , WHITE=-2};

class Map {
public:
    Map(unsigned int width, unsigned int height, std::vector<std::tuple<int, int, int> > numbers); // numbers: (x, y, number)
    void print_map() const;         // paints the map, 0 = empty, -1 = black, positive number = island size
    void draw_solution(const std::vector<Island> &solution) const;
    int get_entry(int index) const;
    int get_size() const { return width * height; }
    int get_height() const { return height; }
    int get_width() const { return width; }
    
    // conversion between coordinates and corresponding vector indices
    int x_coord(int index) const;
    int y_coord(int index) const;
    int get_index(int x, int y) const;

	std::vector<int> neighbours(int index) const;    // returns indices of adjacent cells
	void neighbours(int index, std::vector<int> &result) const;    // return value as output parameter for efficiency
	const std::vector<int> &numbered_indices() const { return numbered_indices_; }

private:
    unsigned int width, height;
    std::vector<int> mapvector;
	std::vector<int> numbered_indices_;
};

#endif /* MAP_H */

