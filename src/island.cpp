#include "island.h"
#include "Map.h"
#include <iostream>
#include <unordered_map>
#include <unordered_set>

using namespace std;

struct PartialPath {
    using Path = vector<int>;

    Path path_so_far;
    unordered_set<int> indices_to_go;
public:
    PartialPath(const unordered_set<int> &indices_to_go)
            : indices_to_go(indices_to_go) {
    }

    PartialPath(const PartialPath &prefix, int continuation)
            : path_so_far(prefix.path_so_far),
              indices_to_go(prefix.indices_to_go) {
        path_so_far.push_back(continuation);
        indices_to_go.erase(continuation);
    }

    bool is_done() const {return indices_to_go.empty();}
    const Path &get_path() const {return path_so_far;}
    const unordered_set<int> get_remaining_indices() const {return indices_to_go;}

    bool operator==(const PartialPath &other) const {
        // We don't compare indices_to_go, assuming compared paths come from the same graph.
        return (path_so_far == other.path_so_far);
    }
};

namespace std {
    template <>
    struct hash<PartialPath> {
        size_t operator()(const PartialPath& partial_path) const {
            hash<int> hasher;
            size_t seed = 0;
            for (int i : partial_path.get_path()) {
                seed ^= hasher(i) + 0x9e3779b9 + (seed<<6) + (seed>>2);
            }
            return seed;
        }
    };
}

Island::Island(vector<int> indices, const Map &map) {
/*
 * indices are the cell indices of the island, starting with the numbered cells.
 * We need to compute all possible routes and store them. As a side effect, we
 * could also store the possible exits.
 */
    PartialPath empty_path(unordered_set<int>(indices.begin(), indices.end()));
    PartialPath path_starting_in_numbered_cell(empty_path, indices[0]);

    unordered_set<PartialPath> current_partial_paths{path_starting_in_numbered_cell};
    unordered_set<PartialPath> next_partial_paths;

    while (!current_partial_paths.empty() && !current_partial_paths.begin()->is_done()) {
        for (const PartialPath &partial_path : current_partial_paths) {
            int last_index = partial_path.get_path().back();
            int x1 = map.x_coord(last_index);
            int y1 = map.y_coord(last_index);
            for (int next_index : partial_path.get_remaining_indices()) {
                int x2 = map.x_coord(next_index);
                int y2 = map.y_coord(next_index);
                if (abs(x1 - x2) + abs(y1 - y2) == 1) {
                    next_partial_paths.insert(PartialPath(partial_path, next_index));
                }
            }
        }
        current_partial_paths.swap(next_partial_paths);
        next_partial_paths.clear();
    }

    unordered_map<int, vector<int>> paths_by_exit;
    for (const PartialPath &completed_path : current_partial_paths) {
        const vector<int> &path = completed_path.get_path();
        paths_by_exit[path.back()] = path;
    }

    possible_routes.reserve(paths_by_exit.size());
    exits.reserve(paths_by_exit.size());
    for (const auto &entry : paths_by_exit) {
        exits.push_back(entry.first);
        possible_routes.push_back(entry.second);
    }
}
