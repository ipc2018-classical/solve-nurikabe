#ifndef SEARCH_H
#define SEARCH_H

#include "smodels.h"
#include <limits>
#include <sstream>
#include <unordered_map>
#include <vector>

class State;
class StateHash;
class Island;
class Map;

class PlanManager {
	std::string plan_filename;
	int num_plans;
	int best_plan_cost;
public:
	PlanManager(const std::string &plan_filename)
			: plan_filename(plan_filename),
			  num_plans(0),
			  best_plan_cost(std::numeric_limits<int>::max()) {
	}

	int get_best_plan_cost() const {
		return best_plan_cost;
	}

	void save_plan(const std::string &plan, int cost);
};

class Search {
    PlanManager &plan_manager;
    // using same vector for all neighbor requests to avoid reallocation cost.
    std::vector<int> neighbors;
public:
    Search(PlanManager &plan_manager)
        : plan_manager(plan_manager) {
    };

    int search(const std::vector<Island> &islands, const Map &map, int robot_index, double w = 1);

private:
    int shortest_path_cost(const std::vector<Island> &islands, int index_from, int index_to, const State &state, const Map &map, std::vector<int> *path=NULL);
    bool is_goal(const State &painted, int num_islands);
	void extract_plan(const Map &map, const std::vector<Island> &islands, int start_pos,
		std::unordered_map<State, std::tuple<int, int>, StateHash>& closed, const State &state,
		int current_island, std::stringstream &plan);
};

#endif /* SEARCH_H */
