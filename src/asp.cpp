#include "api.h"
#include "asp.h"
#include "atomrule.h"
#include "island.h"
#include "Map.h"
#include "smodels.h"
#include <iostream>
#include <vector>

using namespace std;


ASP::ASP(const Map &map)
    : map(map),
      num_models(0),
      num_valid(0) {
    Api api(&smodels.program);
    create_program(api);
    smodels.init();
}


Painting ASP::next_consistent_painting() {
    while (smodels.model()) {  // Returns 0 when there are no more models
        ++num_models;
        if (num_models % 1000 == 0) {
            cout << "Found " << num_models << " paintings (" << num_valid << " with Hamilton paths)" << endl;
        }
        bool solution_valid = true;
        Painting painting;
        for (size_t i = 0; i < map.numbered_indices().size(); ++i) {
            int index = map.numbered_indices()[i];
            vector<int> indices = {index};
            for (unsigned int j = 0; j < map.get_size(); ++j) {
                // We could have a limited loop that only ranges over nearby cells.
                if (j == index) continue;
                if (connected_white[index][j]->Bpos or connected_white[j][index]->Bpos)
                    indices.push_back(j);
            }
            Island island(indices, map);
            if (island.get_possible_routes().empty()) {
                // Solutions can be invalid because the constraints that guarantee
                // a Hamilton path are not strict enough to cover all cases.
                solution_valid = false;
                break;
            }
            painting.push_back(island);
        }
        if (solution_valid) {
            ++num_valid;
            return painting;
        }
    }
    return Painting();
}


void ASP::setup_atoms(Api &api) {
    int size = map.get_size();
    white_cells.resize(size);
    black_cells.resize(size);
    numbered_cells.resize(size);
    w_connected.resize(size);
    sqr_black.resize(size);
    at_least_two_black_neighbours.resize(size);
    for (unsigned int index = 0; index < size; ++index) {
        white_cells[index] = api.new_atom ();  
        black_cells[index] = api.new_atom ();  
        numbered_cells[index] = api.new_atom ();  
        w_connected[index] = api.new_atom ();  
        sqr_black[index] = api.new_atom ();
        at_least_two_black_neighbours[index] = api.new_atom ();

        // give atoms names (just for printing)
        string coordinate = "(" + to_string(map.x_coord(index)) +
                                 ", " + to_string(map.y_coord(index)) + ")";
        api.set_name(white_cells[index], const_cast<char*>(("white" + coordinate).c_str()));
        api.set_name(black_cells[index], const_cast<char*>(("black" + coordinate).c_str()));
        api.set_name(numbered_cells[index], const_cast<char*>(("numbered" + coordinate).c_str()));
        api.set_name(numbered_cells[index], const_cast<char*>(("at_least_two_black_neighbours" + coordinate).c_str()));
    }

    //vector of vectors keeping track of adjacent and orthogonally connected cells
    connected_white.resize(size);
    connected_black.resize(size);
    for (unsigned int index1 = 0; index1 < size; ++index1) {
        string coordinate1 = "(" + to_string(map.x_coord(index1)) +
                                 ", " + to_string(map.y_coord(index1)) + ", ";
        connected_white[index1].resize(size);
        connected_black[index1].resize(size);
        for (unsigned int index2 = 0; index2 < size; ++index2) {
            connected_white[index1][index2] = api.new_atom ();  
            connected_black[index1][index2] = api.new_atom ();  
            
            // give atoms names (just for printing)
            string coordinates = coordinate1 + to_string(map.x_coord(index2)) +
                                      ", " + to_string(map.y_coord(index2)) + ")";
            api.set_name(connected_white[index1][index2], const_cast<char*>(("connected_white" + coordinates).c_str()));
            api.set_name(connected_black[index1][index2], const_cast<char*>(("connected_black" + coordinates).c_str()));
        }
    }
}


void ASP::create_program(Api &api) {
    setup_atoms(api);

    // Keep some cells white
    api.begin_rule(CHOICERULE);
    for (int i = 0; i < white_cells.size(); ++i)
        api.add_head(white_cells[i]);
    api.end_rule();

    for (int i = 0; i < map.get_size(); ++i) {
        // N7: if cell is not white then it is black
        api.begin_rule (BASICRULE);
        api.add_head(black_cells[i]);
        api.add_body(white_cells[i], false);
        api.end_rule ();

        if (map.get_entry(i) > 0) { // numbered cell
            //add its index to the list of numbered cells

            /*N1: Numbered cells remain white.
            :- not cell(w,X,Y), numbered(X,Y,N).*/
            //this rule only exists for numbered cells
            api.begin_rule (BASICRULE);
            Atom *new_atom = api.new_atom ();
            aux_atoms.push_back(new_atom);
            api.add_head(new_atom);
            api.add_body(new_atom, false);
            api.add_body(white_cells[i], false);
            api.end_rule ();

            //set cell to numbered
//            api.set_name(numbered_cells[i], "certainly numbered");
            api.begin_rule (BASICRULE);
            api.add_head(numbered_cells[i]);
            api.end_rule ();
        }
    
        /*N2 & N5 : With N7, every white cell is orthogonally connected to some numbered cell.
                        :- cell(w,X,Y), not w_connected(X,Y), row(X), col(Y).
        adj(C,R,C1,R1) :- col(C;C1), row(R;R1), abs(C-C1)+abs(R-R1)==1.
        connected(C,X,Y,X,Y) :- cell(C,X,Y), row(X), col(Y), color(C).
        connected(C,X,Y,U,V) :- connected(C,X,Y,W,Z), adj(W,Z,U,V), cell(C,U,V), row(X;U;W), col(Y;V;Z), color(C).
        w_connected(X,Y) :- connected(w,X,Y,U,V), cell(w,X,Y), numbered(U,V,N), row(X), col(Y).*/ 
        api.begin_rule (BASICRULE);
        Atom *new_atom = api.new_atom ();
        aux_atoms.push_back(new_atom);
        api.add_head(new_atom);
        api.add_body(new_atom, false);
        api.add_body(w_connected[i], false);
        api.add_body(white_cells[i], true);
        api.end_rule ();

        api.begin_rule (BASICRULE);
        api.add_head(connected_white[i][i]);
        api.add_body(white_cells[i], true);
        api.end_rule ();
        api.begin_rule (BASICRULE);
        api.add_head(connected_black[i][i]);
        api.add_body(black_cells[i], true);
        api.end_rule ();
       
        for (int j = 0; j < map.get_size(); ++j) {
            vector<int> &&adjacent_cells = map.neighbours(j);
        
            for (int n : adjacent_cells) {
                api.begin_rule (BASICRULE);
                api.add_head(connected_black[i][j]);
                api.add_body(connected_black[i][n], true);
                api.add_body(black_cells[j], true);
                api.end_rule ();
                api.begin_rule (BASICRULE);
                api.add_head(connected_white[i][j]);
                api.add_body(connected_white[i][n], true);
                api.add_body(white_cells[j], true);
                api.end_rule ();
            } 
           
            // TODO Gabi: we could get rid of the numbered cells atoms as they are
            // grounded anyways. In this case, we need to determine the numbered
            // cells before the loop.
            api.begin_rule (BASICRULE);
            api.add_head(w_connected[i]);
            api.add_body(connected_white[i][j], true);
            api.add_body(white_cells[i], true);
            api.add_body(numbered_cells[j], true);
            api.end_rule ();
            
            /* N7: All black cells should be connected orthogonally
            :- not connected(b,X,Y,U,V), cell(b,X,Y), cell(b,U,V), row(X;U), col(Y;V).
            cell(b,X,Y) :- not cell(w,X,Y), row(X), col(Y).*/
            api.begin_rule (BASICRULE);
            Atom *new_atom = api.new_atom ();
            aux_atoms.push_back(new_atom);
            api.add_head(new_atom);
            api.add_body(new_atom, false);
            api.add_body(connected_black[i][j], false);
            api.add_body(black_cells[i], true);
            api.add_body(black_cells[j], true);
            api.end_rule ();
        }
    }

    /*N3 & N6: With N2, two numbered cells can not be orthogonally connected.
    :- numbered(X,Y,N), numbered(U,V,M), X<U, connected(w,X,Y,U,V).
    :- numbered(X,Y,N), numbered(U,V,M), Y<V, connected(w,X,Y,U,V).*/ 
    for (int i = 0; i < map.numbered_indices().size() - 1; ++i) {
        for (int j = i+1; j < map.numbered_indices().size(); ++j) {
            api.begin_rule (BASICRULE);
            Atom *new_atom = api.new_atom ();
            aux_atoms.push_back(new_atom);
            api.add_head(new_atom);
            api.add_body(new_atom, false);
            int index1 = map.numbered_indices()[i];
            int index2 = map.numbered_indices()[j];
            api.add_body(connected_white[index1][index2], true);
            api.end_rule ();
        }
    }

    /*
      N4: Each island should contain the same number of white cells as the number it contains, i.e., a white cell numbered N is connected to exactly N white cells:
      island(X,Y) :- N {connected(w,X,Y,U,V):row(U):col(V)} N, numbered(X,Y,N).
      :- not island(X,Y), numbered(X,Y,N).
    */
    for (int i = 0; i < map.numbered_indices().size(); ++i) {
        Atom *island_lower = api.new_atom();
        aux_atoms.push_back(island_lower);
        Atom *island_upper = api.new_atom();
        aux_atoms.push_back(island_upper);
        int index = map.numbered_indices()[i];
        string name_upper = "upper(" + to_string(map.x_coord(index)) + ", " + to_string(map.y_coord(index)) + ")";
        api.set_name(island_upper, const_cast<char*>(name_upper.c_str()));
        string name_lower = "lower(" + to_string(map.x_coord(index)) + ", " + to_string(map.y_coord(index)) + ")";
        api.set_name(island_lower, const_cast<char*>(name_lower.c_str()));
        int value = map.get_entry(index);

        // at least
        api.begin_rule (CONSTRAINTRULE);
        api.add_head(island_lower);
        for (unsigned int j = 0; j < connected_white[index].size(); ++j) {
            api.add_body(connected_white[index][j], true);
        }
        api.set_atleast_body(value);   
        api.end_rule ();
  
        // at most (cf. PhD thesis Patrik Simons page 9)
        api.begin_rule (CONSTRAINTRULE);
        api.add_head(island_upper);
        for (unsigned int j = 0; j < connected_white[index].size(); ++j) {
            api.add_body(connected_white[index][j], false);
        }
       
        int bound = -value + connected_white[index].size();
        api.set_atleast_body(bound);
        api.end_rule ();
        
        // must respect upper and lower bounds
        Atom *new_atom = api.new_atom ();
        aux_atoms.push_back(new_atom);
        api.begin_rule (BASICRULE);
        api.add_head(new_atom);
        api.add_body(new_atom, false);
        api.add_body(island_lower, false);
        api.end_rule ();
        Atom *other_new_atom = api.new_atom ();
        aux_atoms.push_back(other_new_atom);
        api.begin_rule (BASICRULE);
        api.add_head(other_new_atom);
        api.add_body(other_new_atom, false);
        api.add_body(island_upper, false);
        api.end_rule ();
    }

    /* N8: No subset of black cells forms a 2x2 square.
    :- sqrBlack(X,Y), row(X), col(Y).
    sqrBlack(X,Y) :- cell(b,X+1,Y), cell(b,X,Y), cell(b,X, Y+1), cell(b,X+1, Y+1), row(X), col(Y).*/
    //size -1 because in a 3x3 field, size is 3, but I want to check at coordinates 0 and 1. Because 2 is edge and cannot have a square to the right
//    for (unsigned int i = 0; i < map.get_width() - 1 ; ++i) {
//        //same in 2nd dimension
//        for(unsigned int j = 0; j < map.get_height() - 1 ; ++j){
//            // must be an island
//            int index = map.get_index(i,j); //cell(b,X,Y)
//            int neighbor1 = map.get_index(i, j + 1);
//            int neighbor2 = map.get_index(i + 1, j);
//            int neighbor3 = map.get_index(i + 1, j + 1);
//            
//            //:- sqrBlack(X,Y), row(X), col(Y).
//            Atom *new_atom = api.new_atom ();
//            aux_atoms.push_back(new_atom);
//            api.begin_rule (BASICRULE);
//            api.add_head(new_atom);
//            api.add_body(new_atom, false);
//            api.add_body(sqr_black[index], true);
//            api.end_rule ();
//            
//            api.begin_rule (BASICRULE);
//            api.add_head(sqr_black[index]);
//            api.add_body(black_cells[index], true);
//            api.add_body(black_cells[neighbor1], true);
//            api.add_body(black_cells[neighbor2], true);
//            api.add_body(black_cells[neighbor3], true);
//            api.end_rule ();
//        }    
//    }

    // The following rules are not from Nurikabe but for ensuring Hamiltonian
    // paths over the islands, starting from the numbered cell

    // each cell with entry 3 must have at least three black neighbours 
    for (auto index: map.numbered_indices()) {
        if (map.get_entry(index) == 3) {
            Atom *hamiltonian = api.new_atom();
            aux_atoms.push_back(hamiltonian);
            api.begin_rule (CONSTRAINTRULE);
            api.add_head(hamiltonian);
            vector<int> neighbours = map.neighbours(index);
            for (int neighbour : neighbours) {
                api.add_body(black_cells[neighbour], true);
            }
            int outside = 4 - neighbours.size(); 
            api.set_atleast_body(3 - outside);
            api.end_rule ();
            Atom *new_atom = api.new_atom ();
            aux_atoms.push_back(new_atom);
            api.begin_rule (BASICRULE);
            api.add_head(new_atom);
            api.add_body(new_atom, false);
            api.add_body(hamiltonian, false);
            api.end_rule ();
        }
    }

    /* 
     * There is a Hamiltonian path on all islands of size 4 except for those
     * with shape (1)
     * xxx
     *  x
     * and (2) the "line shaped ones" (everything else except the square), where
     * the number is not at an end. 
     *
     * To exclude (1), we require that every cell on the island must have at
     * least two neighbouring black cells. To exclude (2), we require that
     * either there are three neighbouring black cells for the number or the
     * island is a square.
     */

    // we count cells "outside the grid" as black
    for (unsigned int index = 0; index < map.get_size(); ++index) {
        api.begin_rule (CONSTRAINTRULE);
        api.add_head(at_least_two_black_neighbours[index]);
        auto neighbours = map.neighbours(index);
        for (int neighbour : neighbours) {
            api.add_body(black_cells[neighbour], true);
        }
        int outside = 4 - neighbours.size(); 
        api.set_atleast_body(2 - outside);   
        api.end_rule ();
    }

    for (auto index: map.numbered_indices()) {
        if (map.get_entry(index) == 4) {
            Atom *nothamiltonian = api.new_atom();
            aux_atoms.push_back(nothamiltonian);
            for (unsigned int j = 0; j < map.get_size(); ++j) {
                api.begin_rule (BASICRULE);
                api.add_head(nothamiltonian);
                api.add_body(at_least_two_black_neighbours[j], false);
                api.add_body(connected_white[index][j], true);
                api.end_rule ();
            }

            Atom *new_atom = api.new_atom ();
            aux_atoms.push_back(new_atom);
            api.begin_rule (BASICRULE);
            api.add_head(new_atom);
            api.add_body(new_atom, false);
            api.add_body(nothamiltonian, true);
            api.end_rule ();


            Atom *at_least_three = api.new_atom ();
            aux_atoms.push_back(at_least_three);
            api.begin_rule (CONSTRAINTRULE);
            api.add_head(at_least_three);
            auto neighbours = map.neighbours(index);
            for (int neighbour : neighbours) {
                api.add_body(black_cells[neighbour], true);
            }
            int outside = 4 - neighbours.size(); 
            api.set_atleast_body(3 - outside);   
            api.end_rule ();

            Atom *square = api.new_atom ();
            aux_atoms.push_back(square);

            int x = map.x_coord(index);
            int y = map.y_coord(index);

            int n1, n2, n3;
            // there are (at most) four possibilities of a square
            if (x < map.get_width() - 1 && y < map.get_height() - 1) {
                n1 = map.get_index(x, y + 1);
                n2 = map.get_index(x + 1, y);
                n3 = map.get_index(x + 1, y + 1);

                api.begin_rule (BASICRULE);
                api.add_head(square);
                api.add_body(white_cells[index], true);
                api.add_body(white_cells[n1], true);
                api.add_body(white_cells[n2], true);
                api.add_body(white_cells[n3], true);
                api.end_rule ();
            }
            if (x > 0 && y < map.get_height() - 1) {
                n1 = map.get_index(x, y + 1);
                n2 = map.get_index(x - 1, y);
                n3 = map.get_index(x - 1, y + 1);

                api.begin_rule (BASICRULE);
                api.add_head(square);
                api.add_body(white_cells[index], true);
                api.add_body(white_cells[n1], true);
                api.add_body(white_cells[n2], true);
                api.add_body(white_cells[n3], true);
                api.end_rule ();

            }
            if (x > 0 && y > 0) {
                n1 = map.get_index(x, y - 1);
                n2 = map.get_index(x - 1, y);
                n3 = map.get_index(x - 1, y - 1);

                api.begin_rule (BASICRULE);
                api.add_head(square);
                api.add_body(white_cells[index], true);
                api.add_body(white_cells[n1], true);
                api.add_body(white_cells[n2], true);
                api.add_body(white_cells[n3], true);
                api.end_rule ();

            }
            if (x < map.get_width() - 1 && y > 0) {
                n1 = map.get_index(x, y - 1);
                n2 = map.get_index(x + 1, y);
                n3 = map.get_index(x + 1, y - 1);

                api.begin_rule (BASICRULE);
                api.add_head(square);
                api.add_body(white_cells[index], true);
                api.add_body(white_cells[n1], true);
                api.add_body(white_cells[n2], true);
                api.add_body(white_cells[n3], true);
                api.end_rule ();

            }

            nothamiltonian = api.new_atom();
            aux_atoms.push_back(nothamiltonian);
            api.begin_rule (BASICRULE);
            api.add_head(nothamiltonian);
            api.add_body(nothamiltonian, false);
            api.add_body(at_least_three, false);
            api.add_body(square, false);
            api.end_rule ();
        }
    }

    api.done();
}
