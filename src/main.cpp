#include <iostream>
#include <sstream>
#include <tuple>
#include <unordered_set>
#include <vector>

#include "asp.h"
#include "Map.h"
#include "search.h"

using namespace std;

Map parse_instance(istream &in) {
    unsigned int width, height;
    in >> width >> height;
    int x, y, number;
    vector<int> mapvector(width*height, Entry::EMPTY);
    vector<tuple<int, int, int> > numbers;
    while (in >> x >> y >> number) {
        cout << x << " " << y << " " << number << endl;
        numbers.push_back(make_tuple(x, y, number));
    }
    return Map(width, height, numbers);
}

int string_to_int(const string &text) {
    istringstream ss(text);
    int result;
    return ss >> result ? result : 0;
}

double string_to_double(const string &text) {
    istringstream ss(text);
    double result;
    return ss >> result ? result : 0;
}

pair<Map, Painting> parse_painting(istream &in) {
    unsigned int width, height;
    in >> width >> height;

    vector<vector<string>> cells(height, vector<string>(width, "."));
    vector<tuple<int, int, int>> numbers;

    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            cin >> cells[y][x];
            int number = string_to_int(cells[y][x]);
            if (number) {
                numbers.push_back(make_tuple(x, y, number));
            }
        }
    }
    Map map(width, height, numbers);
    Painting painting;
    for (tuple<int, int, int> start : numbers) {
        unordered_set<int> island_cell_set;
        vector<int> island_cells;
        int start_index = map.get_index(get<0>(start), get<1>(start));
        vector<int> queue(1, start_index);
        island_cells.push_back(start_index);
        while (!queue.empty()) {
            int c = queue.back();
            queue.pop_back();
            for (int n : map.neighbours(c)) {
                if (cells[map.y_coord(n)][map.x_coord(n)] == "." && island_cell_set.count(n) == 0) {
                    island_cell_set.insert(n);
                    island_cells.push_back(n);
                    queue.push_back(n);
                }
            }
        }
        painting.emplace_back(island_cells, map);
    }

    return make_pair(map, painting);
}

int compute_solutions(const Map &map, int stop_after) {
    ASP asp(map);
    PlanManager plan_manager("sas_plan");
    int num_solutions = 0;
    while (num_solutions < stop_after) {
        Painting painting = asp.next_consistent_painting();
        if (painting.empty()) {
            cout << "Explored all possible paintings" << endl;
            break;
        }

        cout << "Valid painting " << ++num_solutions << ":" << endl;
        map.draw_solution(painting);

        Search search(plan_manager);
        search.search(painting, map, 0);
    }
    return plan_manager.get_best_plan_cost();
}


int main(int argc, char **argv) {
    int stop_after = numeric_limits<int>::max();

    if (argc >= 2 && string(argv[1]) == "--start-from-painting") {
        double w = 1;
        if (argc >= 3) {
            w = string_to_double(argv[2]);
        }
        pair<Map, Painting> res = parse_painting(cin);
        Map map = res.first;
        Painting painting = res.second;
        for (const Island &island : painting) {
            if (island.get_possible_routes().empty()) {
                cerr << "Island in given painting has no Hamilton path" << endl;
                exit(1);
            }
        }
        cout << "Map:" << "\n";
        map.print_map();
        map.draw_solution(painting);
        PlanManager plan_manager("sas_plan");
        Search search(plan_manager);
        search.search(painting, map, 0, w);
        int best_plan_cost = plan_manager.get_best_plan_cost();
        if (best_plan_cost < numeric_limits<int>::max()) {
            cout << "Best discovered solution cost: " << best_plan_cost << endl;
        }
    } else if (argc >= 2 && string(argv[1]) == "--check-hamilton-path") {
        pair<Map, Painting> res = parse_painting(cin);
        Painting painting = res.second;
        for (const Island &island : painting) {
            if (island.get_possible_routes().empty()) {
                cerr << "Island in given painting has no Hamilton path" << endl;
                exit(1);
            }
        }
        cout << "All islands have a Hamilton path. The instance is probably solvable.\n";
        exit(0);
    } else {
        if (argc >= 2 && string(argv[1]) == "--suboptimal") {
            if (argc >= 3) {
                stop_after = string_to_int(argv[2]);
                if (stop_after == 0) {
                    cerr << "Expected a non-zero number after --suboptimal but got: " << argv[2] << endl;
                }
            } else {
                stop_after = 1;
            }
            cout << "Looking for any solution" << endl;
        } else {
            cout << "Looking for an optimal solution" << endl;
        }
        Map map = parse_instance(cin);
        cout << "Map:" << "\n";
        map.print_map();

        int best_plan_cost = compute_solutions(map, stop_after);
        if (best_plan_cost < numeric_limits<int>::max()) {
            cout << "Best discovered solution cost: " << best_plan_cost << endl;
        }
    }
}
