#ifndef ISLAND_H
#define ISLAND_H

#include <vector>

class Map;

class Island {

public:
    Island(std::vector<int> indices, const Map &map);
    int entry_point() const { return possible_routes[0][0]; };
    const std::vector<int> &possible_exits() const { return exits; }
    const std::vector<std::vector<int>>& get_possible_routes() const { return possible_routes; }
    int paint_cost() const { return possible_routes[0].size() + 1; }

private:
    std::vector<std::vector<int>> possible_routes;
    std::vector<int> exits;
};
#endif /* ISLAND_H */
