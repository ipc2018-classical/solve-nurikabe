﻿#include "Map.h"
#include "asp.h"
#include "search.h"
#include <iostream>

using namespace std;

Map::Map(unsigned int width, unsigned int height, vector<tuple<int, int, int> > numbers)
    : width(width), height(height) {
    mapvector.resize(width * height, Entry::EMPTY);
    int x, y, number;
    for (auto entry : numbers) {
        tie(x, y, number) = entry;
		int index = get_index(x,y);
        mapvector[index] = number;
		numbered_indices_.push_back(index);
    }
}

int Map::x_coord(int index) const {
    return index % width;
}

int Map::y_coord(int index) const {
    return index / width;
}

int Map::get_index(int x, int y) const {
    return y * width + x;
}

vector<int> Map::neighbours(int index) const {
    vector<int> adjacent_cells;
    neighbours(index, adjacent_cells);
    return adjacent_cells;
}

void Map::neighbours(int index, vector<int> &result) const {
    result.clear();
    int x = x_coord(index);
    int y = y_coord(index);

    if (x > 0)
        result.push_back(get_index(x - 1, y));
    if (y > 0)
        result.push_back(get_index(x, y - 1));
    if (x < width - 1)
        result.push_back(get_index(x + 1, y));
    if(y < height-1)
        result.push_back(get_index(x, y + 1));
}

void Map::print_map() const { //0 = empty, -1 = black, positive number = island size
    char BLACK = 219;
    char WHITE = 32;  
    for (int j = 0; j < height; ++j) {
        for (int i = 0 ; i < width; ++i) {
            if (mapvector[get_index(i,j)] == Entry::BLACK) {
                cout << "\u25A0";
            } else if (mapvector[get_index(i,j)] == Entry::EMPTY) {
                cout << "0 ";
            } else if (mapvector[get_index(i,j)] == Entry::WHITE) {
                cout << "\u2327" << " ";
            } else {
                //anything thats not empty, black or white gets treated as an island number
                cout << mapvector[get_index(i,j)] << " ";
            }
        }
        cout << "\n";
    } 
}

int Map::get_entry(int index) const {
    return mapvector[index];
}

void Map::draw_solution(const vector<Island> &solution) const {
    vector<char> all_cells(get_size());
    for (const Island &island : solution)
        for (auto index : island.get_possible_routes()[0])
            all_cells[index] = 1;
    for (int j = 0; j < get_height(); ++j) {
        for (int i = 0 ; i < get_width(); ++i) {
            if (all_cells[get_index(i, j)] == 1)
                cout << "W";
            else cout << "B";
        }
        cout << endl;
    }
}
