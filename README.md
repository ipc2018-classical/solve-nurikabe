# Domain-specific Solver for the IPC Domain Nurikabe

* Marisa Pfister <m.pfister@unibas.ch>
* Gabriele R�ger <gabriele.roeger@unibas.ch>
* Florian Pommerening <florian.pommerening@unibas.ch>
* Alvaro Torralba <torralba@cs.uni-saarland.de>
